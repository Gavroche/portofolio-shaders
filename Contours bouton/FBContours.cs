using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Animation))]
[RequireComponent(typeof(RectTransform))]
public class FBContours : MonoBehaviour
{
    [Header("Paramètres")]
    public float marge;
    public float progression;

    [Header("Config")]
    public RectTransform rectParent;

    Material mat;
    Animation anim;

    void Awake(){
        Vector2 dimensionsDepart = new Vector2(rectParent.rect.width, rectParent.rect.height);
        float offset = (GetComponent<RectTransform>().offsetMax.x) * 2 - marge;
        mat = GetComponent<Image>().material;
        mat.SetVector("_depart", dimensionsDepart);
        mat.SetVector("_arrivee", dimensionsDepart + new Vector2(offset, offset));

        anim = GetComponent<Animation>();
    }

    void Update(){
        mat.SetFloat("_progression", progression);
    }

    public void OnClick(){
        anim.Stop();
        anim.Play();
    }
}
